﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] vector = new int[n];

            for (int i=0; i<n; i++)
                 vector[i]= int.Parse(Console.ReadLine());
            
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());

            int k = 0;
            int sum = 0;
            for (int i=0; i<n; i++)
                if ((vector[i]>=a && vector[i]<=b) || (vector[i] <= a && vector[i] >= b))
                {
                    sum += vector[i];
                    k++;
                }
            double media = (double)sum / k; //calc media
            Console.WriteLine("Media elementelor este " + media);

            Console.ReadKey();
        }
    }
}
